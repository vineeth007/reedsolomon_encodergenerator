/*
  * reedencoder.cpp
  *@breif : In this file encoder output is auto generated in to output file based on the reedsolomon codec you select
  *  Created on: 20-Mar-2018
  *      Author: shivujagga
  */
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <itpp/itcomm.h>

using namespace itpp;
using namespace std;

void printoptions(void)
{
	cout << "Please select one of the codec for reedsolomon encoding" << endl;
	cout << "-----------------------------------------------------" << endl;
	cout << "OPTION ||CODEC(N,Err)|| INPUT LENGTH || OUTPUT LENGTH" << endl;
	cout << " 1     || (65,9)     ||      56      ||      155     " << endl;
	cout << " 2     || (108,3)    ||      96      ||      180     " << endl;
	cout << " 3     || (100,5)    ||      88      ||      300     " << endl;
	cout << " 4     || (95,6)     ||      80      ||      155     " << endl;
	cout << " 5     || (130,9)    ||      120     ||      310     " << endl;
	cout << " 6     || (70,12)    ||      56      ||      310     " << endl;
	cout << " 7     || (90,11)    ||      80      ||      310     " << endl;
	cout << " 8     || (110,10)   ||      96      ||      310     " << endl;
	cout << " 9     || (108,6)    ||      96      ||      540     " << endl;
	cout << "-----------------------------------------------------" << endl;
}


bvec printreedencoderoutput(int n,int c,int e)
{
	int l = n-8;
	l=l-(l%8);
	bvec input(n);
	//double num;
	FILE *myFile;
	myFile = fopen("input.txt", "r");
	int data;
	for(int i=0;i<l;i++)
	{
		fscanf(myFile, "%d", &data);
		if(data==-1)
		{
			input[i]=0;
		}	
		else
		{
			input[i]=1;
		}
	}
	int xo[8];
	for(int i=0;i<8;i++)
	{
		xo[i] = input[i] xor input[i+8];
	}
	for(int i=16;i<l;i++)
	{
		xo[i%8]=xo[i%8] xor input[i];
	}
	for(int i=l;i<l+8;i++)
	{
		input[i] = xo[i%8];

	}
	for(int i=l+8;i<n;i++)
	{
		input[i]=1;
	}

	bvec output;
	Reed_Solomon REED(c,e);
	output = REED.encode(input);
	//cout << "Reed output size :"<< output.size() << endl;
	for(int i=0;i<output.size();i++)
	{
		if(output[i] == 0)
		{
			cout << "-1 ";
		}
		else
		{
			cout << "1 ";
		}

	}
	//bvec backput = REED.decode(output);
	//for(int i=0;i<backput.size();i++)
	//{
	//	cout << backput[i] << " ";
	//}
	FILE *fp;
	cout << "\nYou want to print data to a text file press 1 and text file name or else press 0" << endl;
	int numm;
	char* filename;
	filename = (char*)malloc(20*sizeof(char));
	cin >> numm;
	if(numm == 1)
	{
		cout << "Please enter output file name" << endl;
		cin >> filename;
	}
	else
	{
		//DoNothing
	}
	fp=fopen(filename,"w");
	for(int i=0;i<output.size();i++) {
		if(output[i] == 0)
			fprintf(fp,"%d ",-1);
		else
			fprintf(fp,"%d ",1);
	}
	fclose(fp);
	free(filename);
	return output;
}
int main()
{
	printoptions();
	int op;
	while(1)
	{
		cin >> op;
		bvec output;
		switch (op) 
		{ 

			case 1: output = printreedencoderoutput(65,5,9); 
				break; 
			case 2: output = printreedencoderoutput(108,4,3); 
				break; 
			case 3: output = printreedencoderoutput(100,4,5); 
				break; 
			case 4: output = printreedencoderoutput(95,5,6); 
				break; 
			case 5: output = printreedencoderoutput(130,5,9); 
				break; 
			case 6: output = printreedencoderoutput(70,5,12); 
				break; 
			case 7: output = printreedencoderoutput(90,5,11); 
				break;
			case 8: output = printreedencoderoutput(110,5,10);
				break;	
			case 9: output = printreedencoderoutput(108,4,6);
				break;
			default: printf("Choose valid option\n");
				 printoptions(); 
				 break;   
		} 
	}
	return 0;
}
